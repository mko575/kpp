/**
 * This is an ANTLR lexing grammar for the K language.
 * It was originally inspired by the definition found in the appendix of the "K Primer" paper of 2014.
 */

lexer grammar KLexer;

import LexBasic;

tokens {
    EOL,
    STRING,
    STR_START, STR_CONTENT, STR_END,
    MODULE, IMPORTS, SYNTAX, CONFIGURATION, CONTEXT, RULE, ENDMODULE,
    MODULE_ID, CELL_ID, ID, WORD,
    CELL_OPEN_OTAG, CELL_OPEN_CTAG, CELL_CLOSE_TAG,
    ANON_VARIABLE, EMPTY_TERM, EMPTY_BAG , EMPTY_SET, EMPTY_MAP, EMPTY_LIST,
    LBRACKET, RBRACKET, LPAREN, RPAREN, LCAST, RCAST, 
    REQUIRES, EQ, COMMA, MAPSTO, SETTO, COLON, THEN, INTO, SLASH
}

channels {
    COMMENT
}

fragment UC_Letter : [A-Z] ;
fragment LC_Letter : [a-z] ;
fragment Letter : ( UC_Letter | LC_Letter ) ;
fragment Digit : DecDigit ;
fragment IdSymbol : [-_'] ;
fragment WordSymbol : IdSymbol | [&#\\@$£%µ?§!*/+<>={};] ;
fragment Symbol : WordSymbol | [.:] ;
fragment FilePathChar : Letter | Digit | Symbol ;
fragment EoL : ('\r'? '\n' | '\r') ;

/******************************************************************************
***   TOKENS COMMON TO MULTIPLE MODES
******************************************************************************/

/******************************* COMMON TOKENS *******************************/
//@ begin block of reused tokens "COMMONS"

//@ begin block of reused tokens "COMMENTS"

KPP_COMMAND : '//#' ( ~[\r\n]* EoL | ( ~[#/] | '#' ~'/' | '/' ~'/' )*? '#//' ) -> channel(HIDDEN);

LATEX_COMMAND : '//@' ~[\r\n]* EoL -> channel(HIDDEN);

COMMENT : ( LineComment | BlockComment ) -> channel(COMMENT) ;

//@ end block of reused tokens "COMMENTS"

// Getting rid of unmeaningfull whitespaces
DISPOSABLE_SPACE : Ws -> skip ;

// Added in order to recognize every token to transform lexing errors into parsing errors
ERROR_CHAR : . ;

//@ end block of reused tokens "COMMONS"

/****************************** STRING TOKENS ********************************/
//@ begin block of reused tokens "STRING"
STRING
    : DQuote ( ~["\r\n] | Esc DQuote )* DQuote
    | SQuote ( ~['\r\n] | Esc SQuote )* SQuote
    -> type(STRING)
    ;
//@ end block of reused tokens "STRING"

/***************************** MODULE-ID TOKENS ******************************/
//@ begin block of reused tokens "MODULE-ID"
MODULE_ID : ( Letter | Digit | IdSymbol )+ -> type(MODULE_ID) ;
//@ end block of reused tokens "MODULE-ID"

/******************************** ID TOKENS **********************************/
//@ begin block of reused tokens "ID"
ID : '!'? UC_Letter ( Letter | Digit | IdSymbol )* -> type(ID) ;
//@ end block of reused tokens "ID"

/******************************* WORD TOKENS *********************************/
//@ begin block of reused tokens "WORD"
WORD : ( LC_Letter | Digit | WordSymbol ) ( Letter | Digit | WordSymbol )* -> type(WORD) ;
// COMMA_WORD : ',' -> type(WORD) ;
//@ end block of reused tokens "WORD"

/***************************** CELL TAG TOKENS *******************************/
//@ begin block of reused tokens "CELL-TAGS"
CELL_CLOSE_TAG : '>' -> type(CELL_CLOSE_TAG), popMode ;
CELL_ID : ( Letter | Digit | '-' | '_' )+ -> type(CELL_ID) ;
//@ end block of reused tokens "CELL-TAGS"

/**************************** TERM TOKENS ****************************/
//@ begin block of reused tokens "TERMS"
//@ begin block of reused tokens "CONSTANT TERMS"
EMPTY_TERM : '.' -> type(EMPTY_TERM) ;
EMPTY_BAG : '.Bag' -> type(EMPTY_BAG) ;
EMPTY_SET : '.Set' -> type(EMPTY_SET) ;
EMPTY_MAP : '.Map' -> type(EMPTY_MAP) ;
EMPTY_LIST : '.List' -> type(EMPTY_LIST) ;
//@ end block of reused tokens "CONSTANT TERMS"
ANON_VARIABLE : '_' -> type(ANON_VARIABLE) ;
//@ begin block of reused tokens "ATTRIBUTES"
LB : '[' -> type(LBRACKET) ;
RB : ']' -> type(RBRACKET) ;
COMMA : ',' -> type(COMMA) ;
LP : '(' -> type(LPAREN) ;
RP : ')' -> type(RPAREN) ;
//@ end block of reused tokens "ATTRIBUTES"
EQ : '=' -> type(EQ) ;
MAPSTO : '|->' -> type(MAPSTO) ;
SETTO : '<-' -> type(SETTO) ;
SLASH : '/' -> type(SLASH) ;
LC : '{' -> type(LCAST) ;
RC : '}:>' -> type(RCAST) ;
COLON : ':' -> type(COLON) ;
THEN : '~>' -> type(THEN) ;
INTO : '=>' -> type(INTO) ;
//@ end block of reused tokens "TERMS"

/******************************************************************************
***   TOP LEVEL MODE
******************************************************************************/

REQUIRE : 'require' -> pushMode(FileRequirements) ;

MODULE : 'module' -> pushMode(Module) ;

//@ insert "COMMONS" prefixed with "TLevel_"

/******************************************************************************
***   MODE: Module
******************************************************************************/

mode FileRequirements ;

FReq_EOL : EoL -> type(EOL), popMode ;

FILE_PATH : ('/')? ( ( '.' | '..' | FilePathChar+ ) '/' )* FilePathChar+ ;

//@ insert "STRING_START" prefixed with "FReq_"

//@ insert "COMMONS" prefixed with "FReq_"

/******************************************************************************
***   MODE: Module
******************************************************************************/

mode Module ;

DIRECT_ENDMODULE : 'endmodule' -> type(ENDMODULE), popMode ;

FIRST_IMPORTS : 'imports' -> type(IMPORTS), pushMode(Import) ;
FIRST_SYNTAX : 'syntax' -> type(SYNTAX), pushMode(Syntax) ;
FIRST_CONFIGURATION : 'configuration' -> type(CONFIGURATION), pushMode(Configuration) ;
FIRST_CONTEXT : 'context' -> type(CONTEXT), pushMode(Context) ;
FIRST_RULE : 'rule' -> type(RULE), pushMode(Rule) ;

//@ insert "MODULE-ID" prefixed with "Mod_"

//@ insert "COMMONS" prefixed with "Mod_"

/******************************************************************************
***   MODE: Import, Syntax, Configuration, Rule
******************************************************************************/

/******************************* MODES TOKENS ********************************/
//@ begin block of reused tokens "MODES"
IMPORTS : 'imports' -> type(IMPORTS), mode(Import) ;
SYNTAX : 'syntax' -> type(SYNTAX), mode(Syntax) ;
CONFIGURATION : 'configuration' -> type(CONFIGURATION), mode(Configuration) ;
CONTEXT : 'context' -> type(CONTEXT), mode(Context) ;
RULE : 'rule' -> type(RULE), mode(Rule) ;
ENDMODULE : 'endmodule' -> type(ENDMODULE), popMode, popMode ;
//@ end block of reused tokens "MODES"

/******************************************************************************
***   MODE: Import
******************************************************************************/

mode Import ;

Imp_EOL : EoL -> type(EOL), popMode ;

Imp_SYNTAX : 'syntax' ;

//@ insert "MODULE-ID" prefixed with "Imp_"

//@ insert "COMMONS" prefixed with "Imp_"

/******************************************************************************
***   MODE: Syntax
******************************************************************************/

mode Syntax ;

//@ insert "MODES" prefixed with "Syn_"

Syn_LBRACKET : '[' -> type(LBRACKET), pushMode(SyntaxAttributes) ;

SYNTAX_DEF : '::=' ;

ALT : '|' | '>' ;

LIST_START : 'List{' ;
Syn_COMMA : ',' -> type(COMMA);
LIST_END : '}' ;

Syn_LPAREN : '(' -> type(LPAREN) ;
Syn_RPAREN : ')' -> type(RPAREN) ;

//@ insert "STRING_START" prefixed with "Syn_"

//@ insert "ID" prefixed with "Syn_"

//@ insert "WORD" prefixed with "Syn_"

//@ insert "COMMONS" prefixed with "Syn_"

mode SyntaxAttributes ;

SynAttr_RBRACKET : ']' -> type(RBRACKET), popMode ;

LATEX_SYNTAX_START : 'latex(' -> pushMode(LatexSyntax) ;

SynAttr_COMMA : ',' -> type(COMMA) ;
SynAttr_LP : '(' -> type(LPAREN) ;
SynAttr_RP : ')' -> type(RPAREN) ;

//@ insert "WORD" prefixed with "SynAttr_"

//@ insert "COMMONS" prefixed with "SynAttr_"

mode LatexSyntax ;
LATEX_SYNTAX_END : ')' -> popMode ;
LATEX_GROUP_START : '{' -> pushMode(LatexGroup) ;
LATEX_CHAR : . ;

mode LatexGroup ;
LATEX_GROUP_END : '}' -> popMode ;
LATEX_SUBGROUP_START : '{' -> type(LATEX_GROUP_START), pushMode(LatexGroup) ;
LATEX_GROUP_CHAR : . -> type(LATEX_CHAR) ;

/******************************************************************************
***   MODE: Configuration
******************************************************************************/

mode Configuration ;

Conf_CELL_OPEN_CTAG : '</' -> type(CELL_OPEN_CTAG), pushMode(Conf_CellTag) ;
Conf_CELL_OPEN_OTAG : '<' -> type(CELL_OPEN_OTAG), pushMode(Conf_CellTag) ;

//@ insert "MODES" prefixed with "Conf_"

CONF_VARIABLE : '$' Letter+ ;

//@ insert "TERMS" prefixed with "Conf_"

//@ insert "ID" prefixed with "Conf_"

//@ insert "COMMONS" prefixed with "Conf_"

mode Conf_CellTag ;

MULTIPLICITY : 'multiplicity' ;
COLOR : 'color' ;
STREAM : 'stream' ;
EQUALS : '=' ;

//@ insert "CELL-TAGS" prefixed with "ConfCellTag_"

//@ insert "STRING_START" prefixed with "ConfCellTag_"

//@ insert "COMMONS" prefixed with "ConfCellTag_"

/******************************************************************************
***   MODE: Context
******************************************************************************/

mode Context ;

//@ insert "MODES" prefixed with "Ctxt_"

Ctxt_REQUIRES : 'requires' -> type(REQUIRES) ;

//@ insert "TERMS" prefixed with "Ctxt_"

Ctxt_WORD : ( Letter | Digit | Symbol )+ -> type(WORD) ;

//@ insert "COMMONS" prefixed with "Ctxt_"

/******************************************************************************
***   MODE: Rule
******************************************************************************/

mode Rule ;

//@ insert "MODES" prefixed with "Rule_"

Rul_REQUIRES : 'requires' -> type(REQUIRES) ;

//@ insert "TERMS" prefixed with "Rule_"

//@ insert "ID" prefixed with "Rule_"

//@ insert "WORD" prefixed with "Rule_"

// Rul_WORD : ( Letter | Digit | Symbol )+ -> type(WORD) ;

//@ insert "COMMONS" prefixed with "Rule_"

/******************************************************************************
***   MODE: String(s)
******************************************************************************/

//@ begin block of reused tokens "STRING_START"
DQSTR_START : DQuote -> type(STR_START), pushMode(DQ_String) ;
SQSTR_START : SQuote -> type(STR_START), pushMode(SQ_String) ;
//@ end block of reused tokens "STRING_START"

mode DQ_String ;
DQSTR_END : DQuote -> type(STR_END), popMode ;
DQSTR_CONTENT : ( ~["\r\n] | Esc DQuote )* -> type(STR_CONTENT) ;

mode SQ_String ;
SQSTR_END : SQuote -> type(STR_END), popMode ;
SQSTR_CONTENT : ( ~['\r\n] | Esc SQuote )* -> type(STR_CONTENT) ;
