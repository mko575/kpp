/**
 * This is an ANTLR grammar for the K language.
 * It is inspired by the definition found in the appendix of the "K Primer" paper of 2014.
 */

parser grammar KParser;


options { tokenVocab = KLexer; }


kFile :	( requirementDecl | moduleDecl )* EOF ;

requirementDecl :
    REQUIRE
    ( filePath=FILE_PATH
    | STR_START filePath=STR_CONTENT STR_END
    )
    EOL?
    ;

moduleDecl : MODULE moduleID=MODULE_ID (moduleStmt)* ENDMODULE ;

moduleStmt : importDecl | syntaxDecl | contextDecl | configurationDecl | ruleDecl ;

/* IMPORTS */

importDecl : IMPORTS (Imp_SYNTAX)? moduleID=MODULE_ID EOL? ;

/* SYNTAX */

syntaxDecl : SYNTAX definedSort=ID '::=' syntaxProduction ( ALT syntaxProduction )* ;

syntaxProduction : syntaxProductionTerm attributes? ;

syntaxProductionTerm : ( syntaxNonTerminal | syntaxTerminal | syntaxList | syntaxOther )+ ;

syntaxNonTerminal : ID ;

syntaxTerminal : STR_START literal=STR_CONTENT STR_END ;

syntaxList : LIST_START ID ( COMMA STR_START separator=STR_CONTENT STR_END )? LIST_END ;

syntaxOther : WORD LPAREN ID ( COMMA ID )* RPAREN ;

/* CONFIGURATION */

configurationDecl : CONFIGURATION configurationCell ;

configurationCell :
    CELL_OPEN_OTAG CELL_ID cellAttribute* CELL_CLOSE_TAG
    ( term | configurationCell+ )
    CELL_OPEN_CTAG CELL_ID CELL_CLOSE_TAG
    ;

cellAttribute : key=cellAttrKey EQUALS STR_START value=STR_CONTENT STR_END ;

cellAttrKey : MULTIPLICITY | COLOR | STREAM | WORD ;

/* CONTEXT */

contextDecl : CONTEXT term ( REQUIRES  required=kTerm )? attributes? ;

/* RULES */

ruleDecl : RULE term ( REQUIRES  required=kTerm )? attributes? ;

/* TERMS */

word : WORD | SLASH | EQ ;

atomicTerm
    : CONF_VARIABLE  #confVariableATerm
    | EMPTY_TERM     #rootAKTerm
    | EMPTY_BAG      #emptyBagATerm
    | EMPTY_SET      #emptySetATerm
    | EMPTY_MAP      #emptyMapATerm
    | EMPTY_LIST     #emptyListATerm
    | ( word | COMMA )          #wordATerm
    | ID             #variableATerm
    | ANON_VARIABLE  #anonVariableATerm
    ;

term : atomicTerm                             #atomTerm
     | CELL_OPEN_OTAG CELL_ID CELL_CLOSE_TAG
        term
        CELL_OPEN_CTAG CELL_ID CELL_CLOSE_TAG #cellTerm
     | term MAPSTO term                       #mapTerm
//     | term LBRACKET term SLASH ID RBRACKET   #substitutionTerm
     | term LBRACKET term ( SLASH | SETTO ) ID RBRACKET #mapUpdateTerm
     | term INTO term                         #replacementTerm
     | term COLON ID                          #typingTerm
     | LCAST term RCAST ID                    #castingTerm
     | LPAREN term RPAREN                     #closedTerm
     | term THEN term                        #computationKTerm
     | term term                              #sequenceTerm
     ;

kTerm : term ;


/* OTHERS */

attributes : LBRACKET ( attribute ( COMMA attribute )* )? RBRACKET ;

attribute
    : key=WORD ( LPAREN parameter=WORD RPAREN )?
    | LATEX_SYNTAX_START
        latexSyntaxDef=( LATEX_CHAR | LATEX_GROUP_START | LATEX_GROUP_END )*
        LATEX_SYNTAX_END
    ;
