package com.gitlab.mko575.kpp.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATNConfigSet;
import org.antlr.v4.runtime.dfa.DFA;

import java.util.BitSet;

import com.gitlab.mko575.kpp.parser.TestHelper.Logger;

import static com.gitlab.mko575.kpp.parser.TestHelper.Logger.logIndentedInfo;
import static com.gitlab.mko575.kpp.parser.TestHelper.Logger.logInfo;

public class TestErrorListener extends DiagnosticErrorListener {

    @Override
    public void syntaxError(
            Recognizer<?, ?> recognizer,
            Object offendingSymbol,
            int line,
            int charPositionInLine,
            String msg,
            RecognitionException e
        )
    {
        logInfo("Syntax error at (" + line + ":" + charPositionInLine + "): " + msg);
        if ( offendingSymbol instanceof CommonToken ) {
            CommonToken os = (CommonToken) offendingSymbol;
            logIndentedInfo("Offending symbol: '" + os.getText() + "' of type " + os.getType());

        } else {
            logIndentedInfo("Class of offending symbol: " + offendingSymbol.getClass());
        }
        if ( e != null ) {
            super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
            logIndentedInfo("Exception: " + e.getClass() + " : " + e.getMessage());
            throw e;
        } else {
            logIndentedInfo("No exception");
            // throw new RecognitionException(msg, recognizer, recognizer.getInputStream(), new ParserRuleContext());
        }
    }

    @Override
    public void reportAmbiguity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, boolean exact, BitSet ambigAlts, ATNConfigSet configs) {
        logInfo("Ambiguity error: " + getDecisionDescription(recognizer, dfa));
        super.reportAmbiguity(recognizer, dfa, startIndex, stopIndex, exact, ambigAlts, configs);
    }

    @Override
    public void reportAttemptingFullContext(Parser recognizer, DFA dfa, int startIndex, int stopIndex, BitSet conflictingAlts, ATNConfigSet configs) {
        logInfo("AttemptingFullContext error: " + getDecisionDescription(recognizer, dfa));
        super.reportAttemptingFullContext(recognizer, dfa, startIndex, stopIndex, conflictingAlts, configs);
    }

    @Override
    public void reportContextSensitivity(Parser recognizer, DFA dfa, int startIndex, int stopIndex, int prediction, ATNConfigSet configs) {
        logInfo("ContextSensitivity error: " + getDecisionDescription(recognizer, dfa));
        super.reportContextSensitivity(recognizer, dfa, startIndex, stopIndex, prediction, configs);
    }
}
