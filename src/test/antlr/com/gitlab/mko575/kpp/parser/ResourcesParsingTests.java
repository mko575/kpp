package com.gitlab.mko575.kpp.parser;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.gitlab.mko575.kpp.parser.TestHelper.Logger;
import static com.gitlab.mko575.kpp.parser.TestHelper.Logger.logInfo;

import static org.junit.jupiter.api.Assertions.*;

// @TestMethodOrder(MethodOrderer.Alphanumeric.class)
// @DisplayNameGeneration(ResourcesParsingTests.MyDNG.class)
public class ResourcesParsingTests {

    /**
     * Test parsing the K definition in the file of the provided test.
     * @param test the test to run
     */
    // @DisplayName("displayName")
    @ParameterizedTest(
            // name = "[{index,choice,9#0{index}|99#{index}}] {argumentsWithNames}"
            // name = "[{index}] {argumentsWithNames}"
            // name = "{arguments}"
    )
    @MethodSource("retrieveKFileNames")
    public void testParsingResourcesFiles(ParseTestFromFile test) {
        logInfo("This test will parse: " + test);
        Logger.incrementIndentation();
        logInfo("test suite root: " + test.getTestSuiteRoot());
        logInfo("test file: " + test.getTestFilePath());
        try {
            KParser parser = assertDoesNotThrow(
                () -> TestHelper.getParserOfFile(test.getTestFilePath())
            );
            ParserRuleContext ctx = null;
            TestHelper.setParserToFailFastMode(parser);
            try { ctx = parser.kFile(); }
            catch (ParseCancellationException e) {
                logInfo("SLL mode can't do it, switching to LL mode.");
                parser = assertDoesNotThrow(
                    () -> TestHelper.getParserOfFile(test.getTestFilePath())
                );
                TestHelper.setParserToTryHardMode(parser);
                ctx = parser.kFile();
            }
            TestHelper.assertDefaultSuccessFrom(ctx);
            TestHelper.assertDefaultSuccessFrom(test.toString(), parser, true);
            logInfo("Test OK", true);
        } finally {
            Logger.decrementIndentation();
            logInfo("");
        }
    }

    /**
     * Returns the collection of K files in the test resource.
     * @return the collection of K files in the test resource.
     */
    private static List<ParseTestFromFile> retrieveKFileNames() {
        String rootPathStr = "./k/official-revision-396a314/tutorial-exemples/";
        Path rootPath = Paths.get(ClassLoader.getSystemResource(rootPathStr).getPath());
        List<ParseTestFromFile> res = new ArrayList<ParseTestFromFile>();
        try {
            res = TestHelper
                    .retrieveFilesInSystemResources(rootPathStr, "glob:**.k")
                    .map( (Path p) -> new ParseTestFromFile(rootPath, p))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            String msg = String.format(
                    "IOException generated while looking for K files into '%1$s': %2$s",
                    rootPathStr,
                    e.getMessage()
            );
            System.err.println(msg);
        }
        res.sort( (t1, t2) -> t1.getTestFilePath().compareTo(t2.getTestFilePath()) );
        return res;
    }

    /**
     * Internal class used to deal with {@code @ParameterizedTest} based on files.
     */
    private static class ParseTestFromFile {
        /**
         * The root of all test files of the test suite this test belongs to.
         */
        private final Path testSuiteRootPath;

        /**
         * The path of the file this test is based on.
         */
        private final Path testFilePath;

        /**
         * Default Constructor
         * @param r "root" of all test files.
         * @param p specific file used for this test (including the root path).
         */
        public ParseTestFromFile(Path r, Path p) {
            this.testSuiteRootPath = r;
            this.testFilePath = p;
        }

        /**
         * Returns the root of the test suite.
         * @return the root of the test suite.
         */
        public Path getTestSuiteRoot() {
            return testSuiteRootPath;
        }

        /**
         * Returns the test file path.
         * @return the test file path.
         */
        public Path getTestFilePath() {
            return testFilePath;
        }

        @Override
        public String toString() {
            return testSuiteRootPath.relativize(testFilePath).toString();
        }
    }

    /*
    static class MyDNG extends DisplayNameGenerator.ReplaceUnderscores {

        @Override
        public String generateDisplayNameForClass(Class<?> testClass) {
            return super.generateDisplayNameForClass(testClass);
        }

        @Override
        public String generateDisplayNameForNestedClass(Class<?> nestedClass) {
            return super.generateDisplayNameForNestedClass(nestedClass) + "...";
        }

        @Override
        public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
            String name = testMethod.getName();
            return name.replace('_', ' ') + '.';
        }

    }
    */
}
