package com.gitlab.mko575.kpp.parser;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.PredictionMode;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.junit.platform.commons.logging.Logger;
import org.junit.platform.commons.logging.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.*;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.junit.jupiter.api.Assertions.*;

public class TestHelper {

    /**
     * Execute in debug mode, i.e. with more outputs.
     */
    protected static final boolean debugMode = true;

    /**
     * Creates a {@link CharStream} for parsing from a {@link String}.
     * @param srcTxt String to parse
     * @return {@link CharStream} corresponding to the {@link String} to parse.
     */
    private static CharStream getCharStreamFromString(String srcTxt) {
        return CharStreams.fromString(srcTxt);
    }

    /**
     * Creates a {@link CharStream} for parsing from a file path.
     * @param srcFileName Path of the file to parse.
     * @return {@link CharStream} corresponding to the file to parse.
     * @throws IOException for the same reasons as {@link CharStreams#fromFileName}.
     */
    private static CharStream getCharStreamFromFileName(String srcFileName) throws IOException {
        return CharStreams.fromFileName(srcFileName);
    }

    /**
     * Returns a (default test) parser for its input.
     * @param lexer lexer providing tokens
     * @return a parser for the provided lexer
     */
    private static KParser getParserForLexer(KLexer lexer) {
        KParser parser = new KParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.addErrorListener(new TestErrorListener());
        return parser;
    }

    /**
     * Returns a parser for its input, starting lexing in the specified mode.
     * @param stream input to parse
     * @param startingMode mode i, which to start lexing.
     * @return a parser for the provided input
     */
    private static KParser getParserOfCharStream(CharStream stream, int startingMode) {
        KLexer lexer = new KLexer(stream);
        lexer.mode(startingMode);
        /* Adding TestErrorListener as lexing error listener triggers an error
         * with unrecognized LexerNoViableAltException. Added the ERROR_CHAR
         * token in the lexer grammar to transform any lexing error into a
         * parsing error.
        ANTLRErrorListener errorListener = new TestErrorListener();
        lexer.removeErrorListeners();
        lexer.addErrorListener(errorListener);
        */
        return getParserForLexer(lexer);
    }

    /**
     * Returns a parser for its input.
     * @param stream input to parse
     * @return a parser for the provided input
     */
    private static KParser getParserOfCharStream(CharStream stream) {
        return getParserOfCharStream(stream, 0);
    }

    /**
     * Returns a parser for its input, starting lexing in the specified mode.
     * @param srcTxt the String to be parsed.
     * @param startingMode mode i, which to start lexing.
     * @return a parser for the provided input.
     */
    static KParser getParserOfString(String srcTxt, int startingMode) {
        CharStream stream = getCharStreamFromString(srcTxt);
        return getParserOfCharStream(stream, startingMode);
    }

    /**
     * Returns a parser for its input.
     * @param srcTxt the String to be parsed.
     * @return a parser for the provided input.
     */
    static KParser getParserOfString(String srcTxt) {
        return getParserOfString(srcTxt, 0);
    }

    /**
     * Returns a parser for the content of the file provided as parameter.
     * @param filePath the file to be parsed.
     * @return a parser for the provided input.
     */
    static KParser getParserOfFile(Path filePath) throws IOException {
        CharStream stream = getCharStreamFromFileName(filePath.toString());
        return getParserOfCharStream(stream);
    }

    /**
     * Configure the provided parser to "fail-fast" mode (
     * {@link PredictionMode#SLL SLL(*) prediction mode} and
     * {@link BailErrorStrategy bail error strategy}). In case of syntax error,
     * parsing with this parser returns a {@link ParseCancellationException}.
     * @param parser the parser to configure
     */
    static void setParserToFailFastMode(KParser parser) {
        parser.getInterpreter().setPredictionMode(PredictionMode.SLL);
        parser.removeErrorListeners();
        parser.setErrorHandler(new BailErrorStrategy());
    }

    /**
     * Configure the provided parser to "try hard" mode (
     * {@link PredictionMode#LL LL(*) prediction mode} and
     * {@link DefaultErrorStrategy default error strategy}).
     * @param parser the parser to configure
     */
    static void setParserToTryHardMode(KParser parser) {
        parser.getInterpreter().setPredictionMode(PredictionMode.LL);
        parser.removeErrorListeners();
        parser.addErrorListener(new TestErrorListener());
        parser.setErrorHandler(new DefaultErrorStrategy());
    }

    /**
     * Assert that the {@link Parser} provided as input ({@code parser}) has gone thru a successful parsing.
     * It checks that:
     * - the next token is EOF (if {@code hasParsedAll} is {@code true});
     * - no errors occured during parsing.
     * @param testName the name of the test.
     * @param parser the parser to check.
     * @param hasParsedAll {@code true} iff all the input is supposed to have been parsed.
     */
    static void assertDefaultSuccessFrom(String testName, Parser parser, boolean hasParsedAll) {
           Logger.logInfo("Test [" + testName + "]:");
           Logger.incrementIndentation();
           Token cToken = parser.getCurrentToken();
           Logger.logInfo("current token: " + cToken);
           Logger.logInfo("number of syntax errors: " + parser.getNumberOfSyntaxErrors(), true);
           Logger.decrementIndentation();

            if ( hasParsedAll && parser.getCurrentToken().getType() != Token.EOF ) {
                Token tok = parser.getCurrentToken();
                String msgInfo =
                        String.format("Parser stopped at (%1$d:%2$d) before '%3$s'",
                                tok.getLine(), tok.getCharPositionInLine(), tok.getText());
                fail("Input has not been fully parsed: " + msgInfo);
            }
            if ( parser.getNumberOfSyntaxErrors() != 0 ) {
                fail("Parsing terminated with " + parser.getNumberOfSyntaxErrors() + " error(s).");
                // Logger.logInfo("Parsing terminated with " + parser.getNumberOfSyntaxErrors() + " error(s).");
            }
    }

    /**
     * Assert that the ParserRuleContext provided as input ({@code ctx}) is the result of a successful parsing.
     * It checks that:
     * - the context provided in input is not Null ;
     * - the context provided in input dos not contain an exception.
     * @param ctx the context to check.
     */
    static void assertDefaultSuccessFrom(ParserRuleContext ctx) {
        assertNotNull(ctx);
        assertNull(ctx.exception);
    }

    /**
     * Test if the provided input string ({@code input}) parses as the non-terminal
     * whose name is provided ({@code nonTerminalName}).
     * @param nonTerminalName the name of the non-terminal to use for parsing.
     * @param input the string to parse.
     * @param twoPasses if set to true, a two passes process is used.
     */
    static void assertSuccessfulParsingOfString(String nonTerminalName, String input, int lexMode, boolean twoPasses) {
        KParser parser = TestHelper.getParserOfString(input, lexMode);
        try {
            Method m = KParser.class.getDeclaredMethod(nonTerminalName);
            ParserRuleContext ctx = null;
            if (twoPasses) {
                setParserToFailFastMode(parser);
                try {
                    ctx = (ParserRuleContext) m.invoke(parser);
                } catch (InvocationTargetException e) {
                    if ( e.getCause() instanceof ParseCancellationException) {
                        // thrown by BailErrorStrategy
                        Logger.logInfo("SLL mode can't do it, switching to LL mode.");
                        parser = TestHelper.getParserOfString(input);
                        setParserToTryHardMode(parser);
                        ctx = (ParserRuleContext) m.invoke(parser);
                    } else {
                        System.err.println("Uncaught exception: " + e.getCause());
                    }
                } catch (Exception e) {
                    System.err.println("Uncaught exception: " + e);
                }
            } else {
                ctx = (ParserRuleContext) m.invoke(parser);
            }
            TestHelper.assertDefaultSuccessFrom(ctx);
            TestHelper.assertDefaultSuccessFrom(nonTerminalName, parser, true);
        } catch (NoSuchMethodException e) {
            fail("The rule name (" + nonTerminalName + ") associated to the test ("
                    + input + ") seems deprecated.");
        } catch(InvocationTargetException e) {
            if ( e.getCause() instanceof LexerNoViableAltException) {
                fail("Lexing error for \"" + input + "\" parsed as " + nonTerminalName
                        + ". The offending token is '"
                        + ((LexerNoViableAltException) e.getCause()) + "'.");
            } else if ( e.getCause() instanceof InputMismatchException) {
                fail("Parsing error for \"" + input + "\" parsed as " + nonTerminalName
                        + ". The offending token is '"
                        + ((InputMismatchException) e.getCause()).getOffendingToken() + "'.");
            } else {
                Throwable cause = e.getCause();
                fail("Seemingly an error in the way ANTLR is used: " + cause + " has been thrown.");
            }
        } catch (IllegalAccessException e) {
            fail("Seemingly an error in the way ANTLR is used (IllegalAccessException).");
        } finally {
            System.err.println();
        }
    }

    /**
     * Retrieve all files in resources starting from {@code path} whose names satisfy {@code filePattern}.
     * @param path path to start from.
     * @param filePattern file pattern pattern to look for ({@link PathMatcher#matches}).
     * @return the collection of matching file paths
     * @throws IOException
     */
    public static Stream<Path> retrieveFilesInSystemResources(String path, String filePattern)
    throws IOException {
        URL resourceURL = ClassLoader.getSystemResource(path);
        Path resourcePath = Paths.get(resourceURL.getPath());
        return retrieveFiles(resourcePath, filePattern, true);
    }

    /**
     * Retrieve all files in {@code dirPath} whose names satisfy {@code filePattern}.
     * @param dirPath path to scan.
     * @param filePattern file path pattern to look for ({@link PathMatcher#matches}).
     * @param recursively proceed recursively with subdirectories if {@code true}
     * @return the collection of matching file paths
     * @throws IOException
     */
    public static Stream<Path> retrieveFiles(Path dirPath, String filePattern, boolean recursively)
    throws IOException {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher(filePattern);
        int maxDepth = recursively ? Integer.MAX_VALUE : 0 ;
        return Files.walk(dirPath, maxDepth).filter(matcher::matches);
        /*
        DirectoryStream<Path> localFilesDS = Files.newDirectoryStream(dirPath, filePattern);
        Stream<Path> localFilesStream = StreamSupport.stream(localFilesDS.spliterator(), false);
        Collection<Path> res = localFilesStream.collect(Collectors.toSet());
        if ( recursively ) {
            DirectoryStream<Path> dirDS =
                    Files.newDirectoryStream(
                            dirPath,
                            (Path p) -> Files.isDirectory(p)
                    );
            for (Path entry : dirDS)
                res.addAll(retrieveFiles(entry, filePattern, recursively));
        }
        return res;
        */
    }

    /**
     * Logging functions for test package.
     */
    static class Logger {

        /*
        private static org.junit.platform.commons.logging.Logger testLogger =
                LoggerFactory.getLogger(TestHelper.class);
        static Consumer<String> infoLogger = (String s) -> testLogger.info(() -> s);
        */

        private static int indentationLevel = 0;

        static Consumer<String> infoLogger = (String s) -> { if (debugMode) System.err.println(s); };

        static void incrementIndentation() {
            indentationLevel += 1;
        }
        static void decrementIndentation() {
            if ( indentationLevel > 0 ) indentationLevel -= 1;
        }

        private static String getLoggingPrefix() {
            return getLoggingPrefix(false);
        }
        private static String getLoggingPrefix(boolean last) {
            String res;
            String angle = last ? "└" : "├";
            if ( indentationLevel > 0 ) {
                res = " │  ".repeat(indentationLevel - 1) + " " + angle + "> ";
                // res = " ║ ".repeat(indentationLevel - 1) + " ╠> ";
            } else {
                res = "";
            }
            return res;
        }

        static void logInfo(String msg) {
            logInfo(msg, false);
        }

        static void logInfo(String msg, boolean last) {
            if ( debugMode )
                System.err.println(getLoggingPrefix(last) + msg);
        }

        static void logIndentedInfo(String msg) {
            incrementIndentation();
            logInfo(msg);
            decrementIndentation();
        }

    }

}
