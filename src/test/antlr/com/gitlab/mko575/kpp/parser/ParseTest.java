package com.gitlab.mko575.kpp.parser;

import java.util.Objects;

public class ParseTest {

    /**
     * Number of instances created so far.
     */
    private static int nbInstances = 0;

    /**
     * Prefix used when automatically generating test names.
     */
    private static String defaultNamePrefix = "";

    /**
     * Unique ID assigned to the test.
     */
    private final int UID;

    /**
     * The name of the test as assigned at creation thru the API.
     */
    private final String assignedTestName;

    /**
     * The name of the non-terminal to test against.
     */
    private final String ntName;

    /**
     * The lexingMode to start in.
     */
    private final int lexingMode;

    /**
     * The text to parse
     */
    private final String text;

    /**
     * If the text can be parsed as the non-terminal whose name is in {@link #ntName}.
     */
    private final boolean parsable;

    /**
     * Basic constructor of a test configuration.
     * @param testName name of the test
     * @param ntName name of the non-terminal to test against.
     * @param lexingMode mode to start lexing in
     * @param text text to parse.
     * @param parsable {@code true} iff the text can be parsed as the non-terminal whose name is in {@link #ntName}.
     */
    public ParseTest(String testName, String ntName, int lexingMode, String text, boolean parsable) {
        nbInstances += 1;
        this.UID = nbInstances;
        this.assignedTestName = testName;
        this.ntName = ntName;
        this.lexingMode = lexingMode;
        this.text = text;
        this.parsable = parsable;
    }

    /**
     * Constructor for a "parsable" test.
     * @param testName name of the test
     * @param ntName name of the non-terminal to test against.
     * @param text text to parse.
     * @param parsable {@code true} iff the text can be parsed as the non-terminal whose name is in {@link #ntName}.
     */
    public ParseTest(String testName, String ntName, String text, boolean parsable) {
        this(testName, ntName, 0, text, parsable);
    }

    /**
     * Constructor for a "parsable" test.
     * @param testName name of the test
     * @param ntName name of the non-terminal to test against.
     * @param text text to parse.
     */
    public ParseTest(String testName, String ntName, String text) {
        this(testName, ntName, text, true);
    }

    /**
     * Constructor for a "parsable" test.
     * @param ntName name of the non-terminal to test against.
     * @param lexMode mode to start lexing in
     * @param text text to parse.
     */
    public ParseTest(String ntName, int lexMode, String text) {
        this(null, ntName, lexMode, text, true);
    }

    /**
     * Constructor for a "parsable" test.
     * @param ntName name of the non-terminal to test against.
     * @param text text to parse.
     */
    public ParseTest(String ntName, String text) {
        this(null, ntName, text);
    }

    /**
     * Setter for the default name prefix to use when automatically generating test names ({@link #defaultNamePrefix}).
     * @param dfltNamePrefix default name prefix to use.
     */
    public static void setDefaultNamePrefix(String dfltNamePrefix) {
        defaultNamePrefix = dfltNamePrefix;
    }

    /**
     * Returns the default name prefix ({@link #defaultNamePrefix}).
     * @return the default name prefix ({@link #defaultNamePrefix}).
     */
    public static String getDefaultNamePrefix() {
        return defaultNamePrefix;
    }

    /**
     * Returns a unique generated name for the test.
     * @param prefix prefix to use to generate a unique name.
     * @return a unique generated name for the test.
     */
    public String getGeneratedUniqueName(String prefix) {
        return (prefix.isEmpty() ? "" : prefix + "_") + String.format("%1$03d", UID);
    }

    /**
     * Returns a unique generated name for the test.
     * @return a unique generated name for the test.
     */
    public String getGeneratedUniqueName() {
        return getGeneratedUniqueName(defaultNamePrefix);
    }

    /**
     * Getter for {@link #assignedTestName}.
     * @return the NT name associated to the test.
     */
    public String getTestName() {
        return Objects.requireNonNullElseGet(assignedTestName, this::getGeneratedUniqueName);
    }

    /**
     * Getter for {@link #ntName}.
     * @return the NT name associated to the test.
     */
    public String getNtName() { return ntName; }

    /**
     * Getter for {@link #lexingMode}.
     * @return the lexing mode to start lexing this test in.
     */
    public int getLexingMode() { return lexingMode; }

    /**
     * Getter for {@link #text}.
     * @return the text to parse.
     */
    public String getText() { return text; }

    /**
     * Getter for {@link #parsable}.
     * @return {@code true} iff {@link #text} can be parsed as the non-terminal whose name is {@link #ntName}.
     */
    public boolean isParsable() { return parsable; }

    @Override
    public String toString() {
        String prefix = getDefaultNamePrefix().isEmpty() ? "" : getDefaultNamePrefix() + "_";
        return getGeneratedUniqueName(prefix + getNtName());
    }
}
