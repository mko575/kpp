package com.gitlab.mko575.kpp.parser;

import org.antlr.v4.runtime.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class BasicParsingTests {

    /**
     * Map of basic tests for "every" non-terminal defined in the grammar.
     */
    private static final List<ParseTest> basicNTTests = List.of(
            new ParseTest("requirementDecl" , "require \"dirpath/filename.k\""),
            new ParseTest("requirementDecl" , "require dirpath/filename.k"),
            new ParseTest("moduleDecl"      , "module MODULEID endmodule"),
            new ParseTest("importDecl", KLexer.Module, "imports MODULE-ID2"),
            new ParseTest("syntaxDecl", KLexer.Module, "syntax NT ::= \"lambda\" Id \".\" Exp | NT_2"),
            new ParseTest(
                    "configurationDecl",
                    KLexer.Module,
                    "configuration <T multiplicity=\"many\" color=\"red\" stream=\"in\"> . </T>"
            ),
            new ParseTest("configurationDecl", KLexer.Module, "configuration <T> <A> . </A> <B> <C> . </C> </B> </T>"),
            new ParseTest("contextDecl", KLexer.Module, "context T"),
            new ParseTest("contextDecl", KLexer.Module, "context T requires K [left(1)]"),
            new ParseTest("ruleDecl", KLexer.Module, "rule foo => bar [structural]")
    );

    /**
     * Runs all the tests defined in {@link BasicParsingTests#basicNTTests}.
     * It checks if the text of the test provided as parameter is fully parsed
     * as an instance of the non-terminal defined in the test provided as parameter.
     * @param t the test to run.
     */
    @ParameterizedTest
    @MethodSource("getBasicNTTests")
    public void test_basicNonTerminalTests(ParseTest t) {
        String nonTerminalName = t.getNtName();
        String testTxt = t.getText();
        int lMode = t.getLexingMode();
        TestHelper.assertSuccessfulParsingOfString(nonTerminalName, testTxt, lMode, true);
    }

    /**
     * Returns the set of basic non-terminal tests to run ({@link #basicNTTests}).
     * @return the set of basic non-terminal tests to run ({@link #basicNTTests}).
     */
    private static List<ParseTest> getBasicNTTests() {
        return basicNTTests;
    }

    /**
     * Basic failing test for requirement declaration.
     */
    @Test public void test_RequirementDeclFailure() {
        KParser parser = TestHelper.getParserOfString("reqire dirpath/filename.k");
        // KParser.RequirementDeclContext ctx = assertDoesNotThrow(parser::requirementDecl);
        InputMismatchException e = assertThrows(InputMismatchException.class, parser::requirementDecl);
        assertEquals(e.getOffendingToken().getText(), "r");
    }
}
